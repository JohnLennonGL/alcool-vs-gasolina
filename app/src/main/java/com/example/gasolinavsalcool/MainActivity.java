package com.example.gasolinavsalcool;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    private EditText alcool;
    private EditText gasolina;
    private Button Verificar;
    private TextView resultado;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        gasolina = findViewById(R.id.gasolinaID);
        alcool = findViewById(R.id.AlcoolID);
        Verificar = findViewById(R.id.BotaoVerificarID);
        resultado = findViewById(R.id.ResultadoID);



//        Verificar.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                String TextoAlcool = PrecoAlcool.getText().toString();
//                String TextoGasolina = PrecoGasolina.getText().toString();
//
//
//                if (TextoAlcool.isEmpty() || TextoGasolina.isEmpty()) {
//                    // Resultado.setText("Insira os preços nos dois campos");
//                    Toast.makeText(MainActivity.this, "Campo vazio", Toast.LENGTH_LONG).show();
//                } else {
//                    Double Alcool = Double.parseDouble(TextoAlcool);
//                    Double Gasolina = Double.parseDouble(TextoGasolina);
//                    double ResultadoTotal = Alcool / Gasolina;
//
//                    if (ResultadoTotal >= 0.7) {
//                        Resultado.setText("É melhor comprar Gasolina");
//
//                    } else {
//                        Resultado.setText("É melhor comprar Alcool");
//                    }
//                }
//            }
//        });
    }


    public void verificar(View view){

        //recuperar valoewa digitados

        String  precoAlcool = alcool.getText().toString();
        String  precoGasolina = gasolina.getText().toString();


        //validar campos digitados
       Boolean camposValidados =  validarCampos(precoAlcool,precoGasolina);

       if(camposValidados){
                  Double valorAlcool = Double.parseDouble(precoAlcool);
                  Double valorGasolina = Double.parseDouble(precoGasolina);
                double resultadoTotal = valorAlcool / valorGasolina;


           if (resultadoTotal >= 0.7) {
                        resultado.setText("É melhor comprar Gasolina");

                    } else {
                        resultado.setText("É melhor comprar Alcool");
                    }

       }else{
           Toast.makeText(this, "Campo vazio", Toast.LENGTH_SHORT).show();
       }



    }

    public Boolean validarCampos (String pAlcool, String pGasolina){

       Boolean camposValidados = true;

       if(pAlcool == null || pAlcool.equals("") ){
           camposValidados = false;
       }
       else if( pGasolina == null || pGasolina.equals("")){
           camposValidados = false;
       }

        return camposValidados;

    }
}
